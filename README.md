# rapid-Mapping

#### 简介
​	rapid-mapping一种实体类映射工具，能够通过Java注解将一个实体类的属性安全地赋值给另一个实体类。只需要添加注解，声明映射关系，在启动过程中，rapid-mapping会自动生成对应的转换器，实现将原对象转换成需要的对象。

#### 使用说明

**1、maven项目** 

```xml
<dependency>
    <groupId>com.gitee.little-miser</groupId>
    <artifactId>rapid-mapping</artifactId>
    <version>1.1.9</version>
</dependency>
```

**SpringBoot项目引入starter**
```xml
<dependency>
    <groupId>com.gitee.little-miser</groupId>
    <artifactId>rapid-mapping-boot-starter</artifactId>
    <version>1.1.9</version>
</dependency>
```

**2、在启动时调用初始化方法（非SpringBoot项目）**

```java
public class DemoApplication {
    public static void main(String[] args) {
        //调用初始化方法
        MappingUtil.init();
    }
}
```

注：当不调用初始化方法是，会在第一次使用工具进行初始化（不建议，会影响第一调用的性能）。

**只需要在Springboot启动类上添加@EnableRapidMapping注解**

```java
@EnableRapidMapping
@SpringBootApplication
public class RapidMappingDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(RapidMappingDemoApplication.class, args);
    }
}
```



**3、创建需要映射转换的类**


```java
@Data
@RapidMapping({UserVo.class,UserDto.class})
public class UserEntity {
    
    @RapidMappingField(value = "name",targetClass = UserVo.class)
    private String userName;
}
```
```java
@Data
@RapidMapping(UserDto.class)
public class UserEntity1 {

    private Integer age;
}
```
```java
@Data
public class UserDto {
    private String userName;
    private Integer age;
}
```
```java
@Data
public class UserVo {
    private String name;
}
```
注：在属性一致时，无需添加 @RapidMappingField注解

**4、调用工具方法进行映射**

```java
//无转换类实例时
UserEntity userEntity = new UserEntity();
userEntity.setName("张三");
UserDto userDto = MappingUtil.convert(UserDto.class, userEntity);

//有转换类实例时
UserVoUserVo userVo = new UserVo();
MappingUtil.convert(userVo, userEntity);

//需转换为列表类时
List<UserEntity> userEntities = new ArrayList<>();
userEntities.add(userEntity);
List<UserVo> userVoList = MappingUtil.convertList(UserVo.class, userEntities);

//需要多个对象转换成一个对象时
UserEntity1 userEntity1 = new UserEntity1();
userEntity.setAge(20);
List<UserVo> userVoList = MappingUtil.convertMoreToOne(UserDto.class, userEntity, userEntity1);
```



**注解的使用**


```java
@RapidMapping({UserAddDto.class, UserEditDto.class, UserDto.class})
@Data
public class UserDto {
	//对特定类映射字段名称不一致时，当targetClass不设置是，为所以需要转换的类都为设置的字段名
    @RapidMappingField(value = "name",targetClass = UserEditDto.class)
    private String userName;
	
    //使用dateFormat 对时间进行类型转换，支持String到Date,LocalDateTime,LocalDate之间互相转换
    @RapidMappingField(dateFormat = DateUtils.FORMAT_SECOND)
    private String creatTime;

    @RapidMappingField(targetClass = UserEditDto.class, dateFormat = DateUtils.FORMAT_MILLISECOND)
    @RapidMappingField(dateFormat = DateUtils.FORMAT_NO_MILLISECOND)
    private LocalDateTime updateTime;
}
```

```java
@Data
public class UserAddDto {
    private String userName;
    private Date creatTime;
    private String updateTime;
}
```

```java
public class UserEditDto {
    private String name;
    private LocalDateTime creatTime;
    private String updateTime;
}
```

```java
UserDto userDto = new UserDto();
userDto.setUserName("张三");
userDto.setUpdateTime(LocalDateTime.now());
userDto.setCreatTime("2023-10-10 10:16:56");

UserAddDto userAddDto = MappingUtil.convert(UserAddDto.class, userDto);
System.out.println(userAddDto);
UserEditDto userEditDto = MappingUtil.convert(UserEditDto.class, userDto);
System.out.println(userEditDto);
```
**包含枚举的转换**
```java
//@RapidMapping(sources = UserVo.class)
//sources属性可以指定UserVo转换成UserEntity
@Data
@RapidMapping(UserDto.class)
public class UserEntity {

    private UserTypeEnmu userType;
}
```
```java
@Data
public class UserDto {

    private Integer userType;
}
```


```java
@Getter
@AllArgsConstructor
public enum UserTypeEnmu {

    ADMIN(1, "管理"),
    TEAM_LEADER(2, "组长"),
    STAFF(3, "员工");
	
    @EnumCode
    private Integer code;
    private String value;
}
```



```java
//支持枚举转其子属性，子属性转枚举，枚举转枚举
UserEntity userEntity = new UserEntity();
userEntity.setUserType(UserTypeEnmu.ADMIN);

UserDto userDto = MappingUtil.convert(UserDto.class, userEntity);
System.out.println(userAddDto);
```

