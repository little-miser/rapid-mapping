package com.gitee.little.miser.exception;

public class RapidMappingException extends RuntimeException {

    private String message;

    public RapidMappingException(String message) {
        super(message);
        this.message = message;
    }

    public RapidMappingException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
