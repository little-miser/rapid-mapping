package com.gitee.little.miser.dto;

import com.gitee.little.miser.commonEnum.FormatEnum;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

/**
 * @Author: wanxuefeng
 * @Description: 映射字段信息
 * @CreateTime: 2023-08-18  13:24
 */
public class MappingFieldInfo {

    /*
     * 源对象获取属性方法名
     * */
    private String sourceReadMethodName;

    /*
     * 目标对象设置属性方法名
     * */
    private String targetWriteMethodName;

    /*
     * 默认值
     * */
    private String defaultValue;

    /*
     * 时间格式化 格式
     * */
    private String dateFormat;

    /*
     * 源对象枚举获取属性方法名
     * */
    private String sourceEnumReadMethodName;

    /*
     * 目标对象枚举获取属性方法名
     * */
    private String targetEnumReadMethodName;

    /*
     * 目标对象枚举的Element
     * */
    private TypeMirror targetEnumMirror;

    /*
     * 目标对象的Element
     * */
    private TypeElement targetClassElement;

    /*
     * 格式化类型
     * */
    private FormatEnum formatType;

    public String getSourceReadMethodName() {
        return sourceReadMethodName;
    }

    public void setSourceReadMethodName(String sourceReadMethodName) {
        this.sourceReadMethodName = sourceReadMethodName;
    }

    public String getTargetWriteMethodName() {
        return targetWriteMethodName;
    }

    public void setTargetWriteMethodName(String targetWriteMethodName) {
        this.targetWriteMethodName = targetWriteMethodName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getSourceEnumReadMethodName() {
        return sourceEnumReadMethodName;
    }

    public void setSourceEnumReadMethodName(String sourceEnumReadMethodName) {
        this.sourceEnumReadMethodName = sourceEnumReadMethodName;
    }

    public String getTargetEnumReadMethodName() {
        return targetEnumReadMethodName;
    }

    public void setTargetEnumReadMethodName(String targetEnumReadMethodName) {
        this.targetEnumReadMethodName = targetEnumReadMethodName;
    }

    public TypeMirror getTargetEnumMirror() {
        return targetEnumMirror;
    }

    public void setTargetEnumMirror(TypeMirror targetEnumMirror) {
        this.targetEnumMirror = targetEnumMirror;
    }

    public TypeElement getTargetClassElement() {
        return targetClassElement;
    }

    public void setTargetClassElement(TypeElement targetClassElement) {
        this.targetClassElement = targetClassElement;
    }

    public FormatEnum getFormatType() {
        return formatType;
    }

    public void setFormatType(FormatEnum formatType) {
        this.formatType = formatType;
    }
}
