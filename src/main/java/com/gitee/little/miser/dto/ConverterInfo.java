package com.gitee.little.miser.dto;

import javax.lang.model.element.TypeElement;
import java.util.List;

/**
 * @Author: wanxuefeng
 * @Description: 转换器信息
 * @CreateTime: 2023-08-16  16:53
 */
public class ConverterInfo {

    /*
     * 源对象的Element
     * */
    private TypeElement sourceClassElement;

    /*
     * 映射关系列表
     * */
    private List<MappingInfo> mappingInfoList;

    public TypeElement getSourceClassElement() {
        return sourceClassElement;
    }

    public void setSourceClassElement(TypeElement sourceClassElement) {
        this.sourceClassElement = sourceClassElement;
    }

    public List<MappingInfo> getMappingInfoList() {
        return mappingInfoList;
    }

    public void setMappingInfoList(List<MappingInfo> mappingInfoList) {
        this.mappingInfoList = mappingInfoList;
    }
}
