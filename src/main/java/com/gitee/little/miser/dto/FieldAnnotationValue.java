package com.gitee.little.miser.dto;

public class FieldAnnotationValue {
    private String value;
    //对应的类
    private String targetClassName;
    //是否忽略
    private boolean ignore = false;
    //时间格式化
    private String dateFormat;
    //由于注解中无法使用复杂对象，默认值暂时只支持 String类型
    private String defaultValue;
    //是否字段必须存在对于关系 默认是不强制要求
    private boolean required;

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTargetClassName() {
        return targetClassName;
    }

    public void setTargetClassName(String targetClassName) {
        this.targetClassName = targetClassName;
    }

    public boolean isIgnore() {
        return ignore;
    }

    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }
}
