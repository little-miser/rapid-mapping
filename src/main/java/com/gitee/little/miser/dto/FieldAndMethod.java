package com.gitee.little.miser.dto;

import com.gitee.little.miser.generator.MethodType;

import javax.lang.model.element.Element;
import java.util.Map;

/**
 * @Author:zyq
 * @Description:
 * @CreateTime: 2024-05-14  23:04
 */
public class FieldAndMethod {
    private Element fieldElement;

    private String methodName;

    private FieldAnnotationValue defaultAnnotationValue;

    private Map<String, FieldAnnotationValue> annotationValueMap;

    private MethodType methodType;

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public FieldAnnotationValue getDefaultAnnotationValue() {
        return defaultAnnotationValue;
    }

    public void setDefaultAnnotationValue(FieldAnnotationValue defaultAnnotationValue) {
        this.defaultAnnotationValue = defaultAnnotationValue;
    }

    public Map<String, FieldAnnotationValue> getAnnotationValueMap() {
        return annotationValueMap;
    }

    public void setAnnotationValueMap(Map<String, FieldAnnotationValue> annotationValueMap) {
        this.annotationValueMap = annotationValueMap;
    }

    public Element getFieldElement() {
        return fieldElement;
    }

    public void setFieldElement(Element fieldElement) {
        this.fieldElement = fieldElement;
    }

    public MethodType getMethodType() {
        return methodType;
    }

    public void setMethodType(MethodType methodType) {
        this.methodType = methodType;
    }
}
