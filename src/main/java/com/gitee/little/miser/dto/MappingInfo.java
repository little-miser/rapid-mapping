package com.gitee.little.miser.dto;

import javax.lang.model.element.TypeElement;
import java.util.List;

/**
 * @Author: wanxuefeng
 * @Description: 映射关系信息
 * @CreateTime: 2023-08-18  11:03
 */
public class MappingInfo {

    /*
     * 源对象的Element
     * */
    private TypeElement sourceClassElement;

    /*
     * 源对象的类名
     * */
    private String sourceClassName;

    /*
     * 目标对象的Element
     * */
    private TypeElement targetClassElement;

    /*
     * 目标对象的类
     * */
    private String targetClassName;

    /*
     * 字段级别映射关系列表
     * */
    private List<MappingFieldInfo> fieldInfoList;

    public TypeElement getSourceClassElement() {
        return sourceClassElement;
    }

    public void setSourceClassElement(TypeElement sourceClassElement) {
        this.sourceClassElement = sourceClassElement;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public TypeElement getTargetClassElement() {
        return targetClassElement;
    }

    public void setTargetClassElement(TypeElement targetClassElement) {
        this.targetClassElement = targetClassElement;
    }

    public String getTargetClassName() {
        return targetClassName;
    }

    public void setTargetClassName(String targetClassName) {
        this.targetClassName = targetClassName;
    }

    public List<MappingFieldInfo> getFieldInfoList() {
        return fieldInfoList;
    }

    public void setFieldInfoList(List<MappingFieldInfo> fieldInfoList) {
        this.fieldInfoList = fieldInfoList;
    }
}
