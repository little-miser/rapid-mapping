package com.gitee.little.miser.commonEnum;

import com.gitee.little.miser.annotation.EnumCode;

/*
 * @description: 格式化类型枚举
 * @author: wanxuefeng
 * @date: 2023/10/9 9:45
 **/
public enum FormatEnum {

    DATE_TO_STRING("Date转换成String"),
    LOCAL_DATE_TO_STRING( "LocalDate转换成String"),
    LOCAL_DATE_TIME_TO_STRING( "LocalDateTime转换成String"),
    STRING_TO_DATE("String转换成Date"),
    STRING_TO_LOCAL_DATE( "String转换成LocalDate"),
    STRING_TO_LOCAL_DATE_TIME( "String转换成LocalDateTime"),
    ENUM_TO_VALUE("枚举转对象属性"),
    VALUE_TO_ENUM("对象属性转枚举"),
    ENUM_TO_ENUM("对象属性转枚举"),
    DEFAULT( "属性相同时不做处理"),
    IGNORE("类型不一致，切暂时无特殊处理方式时，跳过此属性映射"),

    ;

    private String describe;

    FormatEnum(String value) {
        this.describe = value;
    }


    public String getDescribe() {
        return describe;
    }
}
