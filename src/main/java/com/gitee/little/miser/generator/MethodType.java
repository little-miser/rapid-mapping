package com.gitee.little.miser.generator;

public enum MethodType {
    READ,
    WRITE
}
