package com.gitee.little.miser.generator;

import com.gitee.little.miser.annotation.RapidMappingCache;
import com.gitee.little.miser.commonEnum.FormatEnum;
import com.gitee.little.miser.dto.ConverterInfo;
import com.gitee.little.miser.dto.MappingFieldInfo;
import com.gitee.little.miser.dto.MappingInfo;
import com.gitee.little.miser.converter.BaseConverter;
import com.gitee.little.miser.utils.DateUtils;
import com.squareup.javapoet.*;

import javax.annotation.processing.Filer;
import javax.lang.model.element.Modifier;
import java.io.IOException;
import java.util.*;

/**
 * @Author: wanxuefeng
 * @Description: 转换器类生成器
 * @CreateTime: 2023-08-15  15:39
 */
public class ConverterClassGenerator {

    private static int suffix = 1;


    public static void createdConverters(Map<Class<?>, ConverterInfo> infoMap, Filer mFiler) {
        infoMap.forEach((clazz, converterInfo) -> {
            converterInfo.getMappingInfoList().forEach(mappingInfo -> {
                createdAndSetJavaCodeNew(mappingInfo, mFiler);
            });
        });
    }


    private static void createdAndSetJavaCodeNew(MappingInfo mappingInfo, Filer mFiler) {

        ClassName sourceClassName = ClassName.get(mappingInfo.getSourceClassElement());
        ClassName targetClassName = ClassName.get(mappingInfo.getTargetClassElement());

        // 根据旧Java类名创建新的Java文件
        MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("convert")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(Object.class, "sourceObj")
                .addParameter(Object.class, "targetObj")
                .addParameter(Boolean.class,"emptyCoverFlag")
                .addStatement("$T source = ($T) sourceObj", sourceClassName, sourceClassName)
                .addStatement("$T target = ($T) targetObj", targetClassName, targetClassName);
        List<MappingFieldInfo> fieldInfoList = mappingInfo.getFieldInfoList();

        fieldInfoList.forEach(mappingFieldInfo -> {
            String defaultValue = mappingFieldInfo.getDefaultValue();
            FormatEnum dateFormatType = mappingFieldInfo.getFormatType();
            String dateFormat = mappingFieldInfo.getDateFormat();
            if (defaultValue != null && !defaultValue.equals("")) {
                methodBuilder.addStatement("target.$L($T.nonNull(source.$L()) ? source.$L() : defaultValue)",
                        mappingFieldInfo.getTargetWriteMethodName(),
                        Objects.class,
                        mappingFieldInfo.getSourceReadMethodName(),
                        mappingFieldInfo.getSourceReadMethodName());
            } else {
                switch (dateFormatType) {
                    case STRING_TO_DATE:
                        methodBuilder.addStatement("if(emptyCoverFlag || $T.nonNull(source.$L())) target.$L($T.nonNull(source.$L()) ? $T.parse(source.$L(), $S) : null)",
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getTargetWriteMethodName(),
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                DateUtils.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                dateFormat);
                        break;
                    case DATE_TO_STRING:
                        methodBuilder.addStatement("if(emptyCoverFlag || $T.nonNull(source.$L())) target.$L($T.nonNull(source.$L()) ? $T.format(source.$L(), $S) : null)",
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getTargetWriteMethodName(),
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                DateUtils.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                dateFormat);
                        break;
                    case STRING_TO_LOCAL_DATE:
                        methodBuilder.addStatement("if(emptyCoverFlag || $T.nonNull(source.$L())) target.$L($T.nonNull(source.$L()) ? $T.parseLocalDate(source.$L(), $S) : null)",
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getTargetWriteMethodName(),
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                DateUtils.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                dateFormat);
                        break;
                    case LOCAL_DATE_TO_STRING:
                        methodBuilder.addStatement("if(emptyCoverFlag || $T.nonNull(source.$L())) target.$L($T.nonNull(source.$L()) ? $T.formatLocalDate(source.$L(), $S) : null)",
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getTargetWriteMethodName(),
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                DateUtils.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                dateFormat);
                        break;
                    case STRING_TO_LOCAL_DATE_TIME:
                        methodBuilder.addStatement("if(emptyCoverFlag || $T.nonNull(source.$L())) target.$L($T.nonNull(source.$L()) ? $T.parseLocalDateTime(source.$L(), $S) : null)",
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getTargetWriteMethodName(),
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                DateUtils.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                dateFormat);
                        break;
                    case LOCAL_DATE_TIME_TO_STRING:
                        methodBuilder.addStatement("if(emptyCoverFlag || $T.nonNull(source.$L())) target.$L($T.nonNull(source.$L()) ? $T.formatLocalDateTime(source.$L(), $S) : null)",
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getTargetWriteMethodName(),
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                DateUtils.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                dateFormat);
                        break;
                    case ENUM_TO_ENUM:
                        methodBuilder.addStatement("if(emptyCoverFlag || $T.nonNull(source.$L())) target.$L($T.nonNull(source.$L()) ? $T.stream($T.values()).filter(e -> e.$L().equals(source.$L().$L())).findFirst().orElse(null) : null)",
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getTargetWriteMethodName(),
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                Arrays.class,
                                ClassName.get(mappingFieldInfo.getTargetEnumMirror()),
                                mappingFieldInfo.getTargetEnumReadMethodName(),
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getSourceEnumReadMethodName());
                        break;
                    case ENUM_TO_VALUE:
                        methodBuilder.addStatement("if(emptyCoverFlag || $T.nonNull(source.$L())) target.$L($T.nonNull(source.$L()) ? source.$L().$L() : null)",
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getTargetWriteMethodName(),
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getSourceEnumReadMethodName());
                        break;
                    case VALUE_TO_ENUM:
                        methodBuilder.addStatement("if(emptyCoverFlag || $T.nonNull(source.$L())) target.$L($T.nonNull(source.$L()) ? $T.stream($T.values()).filter(e -> e.$L().equals(source.$L())).findFirst().orElse(null) : null)",
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getTargetWriteMethodName(),
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                Arrays.class,
                                ClassName.get(mappingFieldInfo.getTargetEnumMirror()),
                                mappingFieldInfo.getTargetEnumReadMethodName(),
                                mappingFieldInfo.getSourceReadMethodName());
                        break;
                    default:
                        methodBuilder.addStatement("if(emptyCoverFlag || $T.nonNull(source.$L())) target.$L(source.$L())",
                                Objects.class,
                                mappingFieldInfo.getSourceReadMethodName(),
                                mappingFieldInfo.getTargetWriteMethodName(),
                                mappingFieldInfo.getSourceReadMethodName());
                }
            }
        });
        AnnotationSpec annotationSpec = AnnotationSpec.builder(RapidMappingCache.class)
                .addMember("source", "$L", mappingInfo.getSourceClassName() + ".class")
                .addMember("target", "$L", mappingInfo.getTargetClassName() + ".class")
                .build();

        StringBuffer classNameBuffer = new StringBuffer();
        classNameBuffer.append(mappingInfo.getSourceClassElement().getSimpleName());
        classNameBuffer.append("To");
        classNameBuffer.append(mappingInfo.getTargetClassElement().getSimpleName());
        classNameBuffer.append("Converter$");
        classNameBuffer.append(suffix);
        //生成类信息
        TypeSpec typeBuilder = TypeSpec.classBuilder(classNameBuffer.toString())
                .addSuperinterface(BaseConverter.class)
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addMethod(methodBuilder.build())
                .addAnnotation(annotationSpec)
                .build();
        suffix++;

        //输出.java文件
        JavaFile javaFile = JavaFile.builder(BaseConverter.class.getPackage().getName(), typeBuilder)
                .addFileComment("Generated code from RapidMapping. Do not modify!")
                .build();
        try {
            javaFile.writeTo(mFiler);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createdConverters(List<ConverterInfo> converterInfoList, Filer mFiler) {
        converterInfoList.forEach(converterInfo -> {
            converterInfo.getMappingInfoList().forEach(mappingInfo -> {
                System.out.println("RapidMappingProcessor sourceClassName:" + mappingInfo.getSourceClassName() + "->targetClassName:" + mappingInfo.getTargetClassName() + " start");
                createdAndSetJavaCodeNew(mappingInfo, mFiler);
                System.out.println("RapidMappingProcessor sourceClassName:" + mappingInfo.getSourceClassName() + "->targetClassName:" + mappingInfo.getTargetClassName() + " end");
            });
        });
    }
}
