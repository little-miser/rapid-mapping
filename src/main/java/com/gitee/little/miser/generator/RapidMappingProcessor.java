package com.gitee.little.miser.generator;

import com.gitee.little.miser.annotation.EnumCode;
import com.gitee.little.miser.annotation.RapidMapping;
import com.gitee.little.miser.annotation.RapidMappingField;
import com.gitee.little.miser.annotation.RapidMappingFieldContainer;
import com.gitee.little.miser.commonEnum.FormatEnum;
import com.gitee.little.miser.dto.*;
import com.google.auto.service.AutoService;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@AutoService(Processor.class)
public class RapidMappingProcessor extends AbstractProcessor {

    private Filer mFiler;
    private Messager mMessager;
    private Types typeUtils;

    private Elements elementUtils;

    /**
     * 初始化
     * 在该初始化方法中, 我们可以获取mFiler/mElementUtils
     * mFiler/mElementUtils是后面生成java文件要用的
     *
     * @param processingEnvironment
     */
    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        mFiler = processingEnvironment.getFiler();
        mMessager = processingEnvironment.getMessager();
        typeUtils = processingEnvironment.getTypeUtils();
        elementUtils = processingEnvironment.getElementUtils();
    }

    /**
     * 此Processor支持的java版本
     *
     * @return
     */
    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_8;
    }

    /**
     * @return 支持的注解类型
     */
    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(RapidMapping.class.getCanonicalName());
    }

    /**
     * 核心方法
     * 获取注解信息, 生成.java的具体逻辑
     *
     * @param set
     * @param roundEnvironment
     * @return
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {

        try {
            Set<? extends Element> elements = roundEnvironment.getElementsAnnotatedWith(RapidMapping.class);
            Map<String, ConverterInfo> converterInfoMap = new HashMap<>();

            for (Element element : elements) {
                if (element.getKind() != ElementKind.CLASS) {
                    continue;
                }
                List<? extends AnnotationMirror> mirrors = element.getAnnotationMirrors();
                for (AnnotationMirror mirror : mirrors) {
                    TypeElement annotationElement = (TypeElement) mirror.getAnnotationType().asElement();
                    Name qualifiedName = (annotationElement).getQualifiedName();
                    if (RapidMapping.class.getName().contentEquals(qualifiedName)) {
                        ExecutableElement requiredKey = mirror.getElementValues().keySet().stream()
                                .filter(key -> "required".contentEquals(key.getSimpleName()))
                                .findFirst()
                                .orElse(null);
                        Boolean requiredValue = Objects.nonNull(requiredKey) && (Boolean) mirror.getElementValues().get(requiredKey).getValue();
                        for (ExecutableElement key : mirror.getElementValues().keySet()) {
                            AnnotationValue annotationValue = mirror.getElementValues().get(key);
                            if ("value".contentEquals(key.getSimpleName())) {
                                if (annotationValue.getValue() instanceof List) {
                                    List<AnnotationValue> annotationValueList = (List<AnnotationValue>) annotationValue.getValue();
                                    for (AnnotationValue value : annotationValueList) {
                                        TypeMirror valueMirror = TypeMirror.class.cast(value.getValue());
                                        createConverterInfo(converterInfoMap, (TypeElement) element, (TypeElement) ((DeclaredType) valueMirror).asElement(), requiredValue);
                                    }
                                } else {
                                    TypeMirror valueMirror = TypeMirror.class.cast(annotationValue.getValue());
                                    createConverterInfo(converterInfoMap, (TypeElement) element, (TypeElement) ((DeclaredType) valueMirror).asElement(), requiredValue);
                                }
                            } else if ("source".contentEquals(key.getSimpleName())) {
                                if (annotationValue.getValue() instanceof List) {
                                    List<AnnotationValue> annotationValueList = (List<AnnotationValue>) annotationValue.getValue();
                                    for (AnnotationValue value : annotationValueList) {
                                        TypeMirror valueMirror = TypeMirror.class.cast(value.getValue());
                                        createConverterInfo(converterInfoMap, (TypeElement) ((DeclaredType) valueMirror).asElement(), (TypeElement) element, requiredValue);
                                    }
                                } else {
                                    TypeMirror valueMirror = TypeMirror.class.cast(annotationValue.getValue());
                                    createConverterInfo(converterInfoMap, (TypeElement) ((DeclaredType) valueMirror).asElement(), (TypeElement) element, requiredValue);
                                }
                            }


                        }
                    }
                }
            }
            List<ConverterInfo> converterInfoList = new ArrayList<>(converterInfoMap.values());
            if (!converterInfoList.isEmpty()) {
                ConverterClassGenerator.createdConverters(converterInfoList, mFiler);
            }
        } catch (Exception e) {
            e.printStackTrace();
            mMessager.printMessage(Diagnostic.Kind.ERROR, "e: " + e);
        }
        return false;
    }

    private void createConverterInfo(Map<String, ConverterInfo> converterInfoMap, TypeElement source, TypeElement target, Boolean requiredValue) {
        String sourceClassName = source.getQualifiedName().toString();
        String targetClassName = target.getQualifiedName().toString();
        ConverterInfo converterInfo = converterInfoMap.get(sourceClassName);

        //判断是否加载过了
        if (Objects.nonNull(converterInfo)) {
            Long count = converterInfo.getMappingInfoList().stream()
                    .filter(mappingInfo -> mappingInfo.getTargetClassName().equals(targetClassName))
                    .count();
            if (count > 0) {
                return;
            }
        }

        if (Objects.isNull(converterInfo)) {
            converterInfo = new ConverterInfo();
            converterInfo.setSourceClassElement(source);
            converterInfo.setMappingInfoList(new ArrayList<>());
            converterInfoMap.put(sourceClassName, converterInfo);
        }

        MappingInfo mappingInfo = new MappingInfo();
        converterInfo.getMappingInfoList().add(mappingInfo);
        mappingInfo.setSourceClassName(sourceClassName);
        mappingInfo.setTargetClassName(targetClassName);
        mappingInfo.setSourceClassElement(source);
        mappingInfo.setTargetClassElement(target);

        List<Element> sourceElementList = new ArrayList<>();
        List<Element> targetElementList = new ArrayList<>();
        addElementList(source, sourceElementList);
        addElementList(target, targetElementList);

        Map<ElementKind, List<Element>> sourceElementMap = sourceElementList.stream()
                .collect(Collectors.groupingBy(Element::getKind));
        Map<ElementKind, List<Element>> targetElementMap = targetElementList.stream()
                .collect(Collectors.groupingBy(Element::getKind));

        List<FieldAndMethod> sourceFieldList = getFieldList(sourceElementMap, source, MethodType.READ);
        List<FieldAndMethod> targetFieldList = getFieldList(targetElementMap, target, MethodType.WRITE);

        createMappingFieldList(mappingInfo, sourceFieldList, targetFieldList, requiredValue);
    }

    private void createMappingFieldList(MappingInfo mappingInfo, List<FieldAndMethod> sourceFieldList, List<FieldAndMethod> targetFieldList, Boolean requiredValue) {

        List<MappingFieldInfo> mappingFieldInfos = new ArrayList<>();
        mappingInfo.setFieldInfoList(mappingFieldInfos);

        for (FieldAndMethod sourceField : sourceFieldList) {
            FieldAnnotationValue sourceAnnotationValue = sourceField.getAnnotationValueMap().get(mappingInfo.getTargetClassName());
            sourceAnnotationValue = Objects.nonNull(sourceAnnotationValue) ? sourceAnnotationValue : sourceField.getDefaultAnnotationValue();
            Element sourceElement = sourceField.getFieldElement();
            String sourceFieldName = sourceElement.getSimpleName().toString();
            List<String> sourceFieldNameList = new ArrayList<>(2);
            sourceFieldNameList.add(sourceFieldName);
            if (Objects.nonNull(sourceAnnotationValue) && Objects.nonNull(sourceAnnotationValue.getValue())) {
                sourceFieldNameList.add(sourceAnnotationValue.getValue());
            }
            //匹配对应的targetField
            List<FieldAndMethod> matchingTargetFieldList = targetFieldList.stream().filter(targetField -> {
                FieldAnnotationValue targetAnnotationValue = targetField.getAnnotationValueMap().get(mappingInfo.getTargetClassName());
                targetAnnotationValue = Objects.nonNull(targetAnnotationValue) ? targetAnnotationValue : targetField.getDefaultAnnotationValue();
                String targetFieldName = targetField.getFieldElement().getSimpleName().toString();
                if (sourceFieldNameList.contains(targetFieldName)) {
                    return true;
                }
                if (Objects.nonNull(targetAnnotationValue) && Objects.nonNull(targetAnnotationValue.getValue())) {
                    if (sourceFieldNameList.contains(targetAnnotationValue.getValue())) {
                        return true;
                    }
                }
                return false;

            }).collect(Collectors.toList());
            if (matchingTargetFieldList.isEmpty()) {
                continue;
            }

            MappingFieldInfo mappingFieldInfo = getMappingFieldInfo(sourceField, sourceAnnotationValue, matchingTargetFieldList, mappingInfo);
            if (Objects.nonNull(mappingFieldInfo)) {
                mappingFieldInfos.add(mappingFieldInfo);
            }
        }
    }

    private MappingFieldInfo getMappingFieldInfo(FieldAndMethod sourceField, FieldAnnotationValue sourceAnnotationValue, List<FieldAndMethod> matchingTargetFieldList, MappingInfo mappingInfo) {
        MappingFieldInfo mappingFieldInfo = new MappingFieldInfo();
        mappingFieldInfo.setSourceReadMethodName(sourceField.getMethodName());

        TypeMirror sourceType = sourceField.getFieldElement().asType();
        //优先寻找类型一致的，当dateFormat不为空时，String、Date、LocalDate、LocalDateTime为类型一致
        //todo 如果两边是Collection这样看内部类型是否一致
        for (FieldAndMethod matchingTargetField : matchingTargetFieldList) {
            FieldAnnotationValue matchingTargetAnnotationValue = matchingTargetField.getAnnotationValueMap().get(mappingInfo.getSourceClassName());
            matchingTargetAnnotationValue = Objects.nonNull(matchingTargetAnnotationValue) ? matchingTargetAnnotationValue : matchingTargetField.getDefaultAnnotationValue();

            Element targetElement = matchingTargetField.getFieldElement();
            TypeMirror targetType = targetElement.asType();

            //忽略此字段
            boolean ignore = getIgnore(sourceAnnotationValue, matchingTargetAnnotationValue);
            if (ignore) {
                continue;
            }

            //类型完全一致（或是其子类）
            if (isSameOrSubtype(sourceType, targetType)) {
                mappingFieldInfo.setTargetWriteMethodName(matchingTargetField.getMethodName());
                String defaultValue = getDefaultValue(sourceAnnotationValue, matchingTargetAnnotationValue);
                mappingFieldInfo.setDefaultValue(defaultValue);
                mappingFieldInfo.setFormatType(FormatEnum.DEFAULT);
                return mappingFieldInfo;
            }

            String dateFormat = getDateFormat(sourceAnnotationValue, matchingTargetAnnotationValue);
            if (Objects.isNull(dateFormat)) {
                continue;
            }
            FormatEnum formatType = getDateFormatType(sourceType, targetType);
            if (Objects.equals(FormatEnum.IGNORE, formatType)) {
                continue;
            }
            mappingFieldInfo.setTargetWriteMethodName(matchingTargetField.getMethodName());
            String defaultValue = getDefaultValue(sourceAnnotationValue, matchingTargetAnnotationValue);
            mappingFieldInfo.setDateFormat(dateFormat);
            mappingFieldInfo.setDefaultValue(defaultValue);
            mappingFieldInfo.setFormatType(formatType);
            return mappingFieldInfo;
        }

        //当未找到类型一致的时，去匹配两边包含枚举，判断枚举中@EnumCode类型是否一致，
        //todo 如果两边是Collection这样看内部枚举是否一致
        for (FieldAndMethod matchingTargetField : matchingTargetFieldList) {
            FieldAnnotationValue matchingTargetAnnotationValue = matchingTargetField.getAnnotationValueMap().get(mappingInfo.getSourceClassName());
            matchingTargetAnnotationValue = Objects.nonNull(matchingTargetAnnotationValue) ? matchingTargetAnnotationValue : matchingTargetField.getDefaultAnnotationValue();

            //忽略此字段
            boolean ignore = getIgnore(sourceAnnotationValue, matchingTargetAnnotationValue);
            if (ignore) {
                continue;
            }

            Element targetElement = matchingTargetField.getFieldElement();
            TypeMirror targetType = targetElement.asType();

            boolean sourceEnumFlag = isEnumOrSubtypeOfEnum(sourceType, elementUtils.getTypeElement("java.lang.Enum"));
            if (sourceEnumFlag) {
                //查询枚举上是否存在获取属性的方法
                Map<ElementKind, ? extends List<? extends Element>> sourceEnumElementMap = ((DeclaredType) sourceType).asElement().getEnclosedElements()
                        .stream()
                        .collect(Collectors.groupingBy(Element::getKind));
                Element sourceEnumFieldElement = sourceEnumElementMap.get(ElementKind.FIELD).stream()
                        .filter(element -> Objects.nonNull(element.getAnnotation(EnumCode.class)))
                        .findFirst()
                        .orElse(null);
                if (Objects.isNull(sourceEnumFieldElement)) {
                    continue;
                }
                String sourceReadMethodName = getReadMethodName(sourceType, sourceEnumFieldElement, sourceEnumElementMap.get(ElementKind.METHOD));
                if (Objects.isNull(sourceReadMethodName)) {
                    continue;
                }

                //判断目标对象属性是也是枚举
                boolean targetEnumFlag = isEnumOrSubtypeOfEnum(targetType, elementUtils.getTypeElement("java.lang.Enum"));
                if (!targetEnumFlag) {
                    //不是枚举时，其属性类型必须相同
                    if (typeUtils.isSameType(targetType, sourceEnumFieldElement.asType())) {
                        mappingFieldInfo.setTargetWriteMethodName(matchingTargetField.getMethodName());
                        String defaultValue = getDefaultValue(sourceAnnotationValue, matchingTargetAnnotationValue);
                        mappingFieldInfo.setSourceEnumReadMethodName(sourceReadMethodName);
                        mappingFieldInfo.setDefaultValue(defaultValue);
                        mappingFieldInfo.setFormatType(FormatEnum.ENUM_TO_VALUE);
                        return mappingFieldInfo;
                    }
                    continue;
                }

                Map<ElementKind, ? extends List<? extends Element>> targetEnumElementMap = ((DeclaredType) targetElement).asElement().getEnclosedElements()
                        .stream()
                        .collect(Collectors.groupingBy(Element::getKind));
                Element targetEnumFieldElement = targetEnumElementMap.get(ElementKind.FIELD).stream()
                        .filter(element -> Objects.nonNull(element.getAnnotation(EnumCode.class)))
                        .findFirst()
                        .orElse(null);
                if (Objects.isNull(targetEnumFieldElement)) {
                    continue;
                }
                String targetReadMethodName = getReadMethodName(sourceType, targetEnumFieldElement, targetEnumElementMap.get(ElementKind.METHOD));
                if (Objects.isNull(targetReadMethodName)) {
                    continue;
                }

                //不是枚举时，其属性类型必须相同
                if (typeUtils.isSameType(targetEnumFieldElement.asType(), sourceEnumFieldElement.asType())) {
                    mappingFieldInfo.setTargetWriteMethodName(matchingTargetField.getMethodName());
                    String defaultValue = getDefaultValue(sourceAnnotationValue, matchingTargetAnnotationValue);
                    mappingFieldInfo.setSourceEnumReadMethodName(sourceReadMethodName);
                    mappingFieldInfo.setTargetEnumReadMethodName(targetReadMethodName);
                    mappingFieldInfo.setTargetEnumMirror(targetType);
                    mappingFieldInfo.setDefaultValue(defaultValue);
                    mappingFieldInfo.setFormatType(FormatEnum.ENUM_TO_ENUM);
                    return mappingFieldInfo;
                }
                continue;
            }

            boolean targetTypeFlag = isEnumOrSubtypeOfEnum(targetType, elementUtils.getTypeElement("java.lang.Enum"));
            if (targetTypeFlag) {
                //查询枚举上是否存在获取属性的方法
                Map<ElementKind, ? extends List<? extends Element>> targetEnumElementMap = ((DeclaredType) targetType).asElement().getEnclosedElements()
                        .stream()
                        .collect(Collectors.groupingBy(Element::getKind));
                Element targetEnumFieldElement = targetEnumElementMap.get(ElementKind.FIELD).stream()
                        .filter(element -> Objects.nonNull(element.getAnnotation(EnumCode.class)))
                        .findFirst()
                        .orElse(null);
                if (Objects.isNull(targetEnumFieldElement)) {
                    continue;
                }
                String targetReadMethodName = getReadMethodName(sourceType, targetEnumFieldElement, targetEnumElementMap.get(ElementKind.METHOD));
                if (Objects.isNull(targetReadMethodName)) {
                    continue;
                }

                //不是枚举时，其属性类型必须相同
                if (typeUtils.isSameType(sourceType, targetEnumFieldElement.asType())) {
                    mappingFieldInfo.setTargetWriteMethodName(matchingTargetField.getMethodName());
                    String defaultValue = getDefaultValue(sourceAnnotationValue, matchingTargetAnnotationValue);
                    mappingFieldInfo.setTargetEnumReadMethodName(targetReadMethodName);
                    mappingFieldInfo.setTargetEnumMirror(targetType);
                    mappingFieldInfo.setDefaultValue(defaultValue);
                    mappingFieldInfo.setFormatType(FormatEnum.VALUE_TO_ENUM);
                    return mappingFieldInfo;
                }
            }
        }
        return null;
    }

    private boolean getIgnore(FieldAnnotationValue sourceAnnotationValue, FieldAnnotationValue targetAnnotationValue) {
        if (Objects.nonNull(sourceAnnotationValue) && sourceAnnotationValue.isIgnore()) {
            return true;
        }
        if (Objects.nonNull(targetAnnotationValue) && targetAnnotationValue.isIgnore()) {
            return true;
        }
        return false;
    }

    private String getReadMethodName(TypeMirror typeMirror, Element fieldElement, List<? extends Element> methodElementList) {
        if (Objects.isNull(methodElementList)) {
            return null;
        }


        List<String> annotationMirrorList = ((DeclaredType )typeMirror).asElement().getAnnotationMirrors().stream()
                .map(Object::toString)
                .collect(Collectors.toList());

        if (annotationMirrorList.contains("@lombok.Data") || annotationMirrorList.contains("@lombok.Getter")) {
            return "boolean".equals(fieldElement.asType().toString()) ? "is" + capitalizeFirstLetter(fieldElement.toString()) : "get" + capitalizeFirstLetter(fieldElement.toString());
        }

        String fieldName = fieldElement.getSimpleName().toString();
        Element methodElement = methodElementList.stream().filter(method -> {
            String methodName = method.getSimpleName().toString();
            return (methodName.equals("get" + capitalizeFirstLetter(fieldName))
                    || methodName.equals("is" + capitalizeFirstLetter(fieldName)))
                    && isSameOrSubtype(((ExecutableElement) method).getReturnType(), fieldElement.asType());
        }).findFirst().orElse(null);
        return Objects.nonNull(methodElement) ? methodElement.getSimpleName().toString() : null;
    }

    private FormatEnum getDateFormatType(TypeMirror sourceType, TypeMirror targetType) {
        //sourceType为字符串时
        if (typeUtils.isSameType(sourceType, elementUtils.getTypeElement("java.lang.String").asType())) {
            if (typeUtils.isSameType(targetType, elementUtils.getTypeElement("java.util.Date").asType())) {
                return FormatEnum.STRING_TO_DATE;
            }
            if (typeUtils.isSameType(targetType, elementUtils.getTypeElement("java.time.LocalDate").asType())) {
                return FormatEnum.STRING_TO_LOCAL_DATE;
            }
            if (typeUtils.isSameType(targetType, elementUtils.getTypeElement("java.time.LocalDateTime").asType())) {
                return FormatEnum.STRING_TO_LOCAL_DATE_TIME;
            }
        }
        if (typeUtils.isSameType(targetType, elementUtils.getTypeElement("java.lang.String").asType())) {
            if (typeUtils.isSameType(sourceType, elementUtils.getTypeElement("java.util.Date").asType())) {
                return FormatEnum.DATE_TO_STRING;
            }
            if (typeUtils.isSameType(sourceType, elementUtils.getTypeElement("java.time.LocalDate").asType())) {
                return FormatEnum.LOCAL_DATE_TO_STRING;
            }
            if (typeUtils.isSameType(sourceType, elementUtils.getTypeElement("java.time.LocalDateTime").asType())) {
                return FormatEnum.LOCAL_DATE_TIME_TO_STRING;
            }
        }
        return FormatEnum.IGNORE;
    }

    private String getDateFormat(FieldAnnotationValue sourceAnnotationValue, FieldAnnotationValue targetAnnotationValue) {
        if (Objects.nonNull(sourceAnnotationValue) && Objects.nonNull(sourceAnnotationValue.getDateFormat()) && !sourceAnnotationValue.getDateFormat().trim().isEmpty()) {
            return sourceAnnotationValue.getDateFormat();
        }
        if (Objects.nonNull(targetAnnotationValue) && Objects.nonNull(targetAnnotationValue.getDateFormat()) && !targetAnnotationValue.getDateFormat().trim().isEmpty()) {
            return targetAnnotationValue.getDateFormat();
        }
        return null;
    }

    private String getDefaultValue(FieldAnnotationValue sourceAnnotationValue, FieldAnnotationValue targetAnnotationValue) {
        if (Objects.nonNull(sourceAnnotationValue) && Objects.nonNull(sourceAnnotationValue.getDefaultValue()) && !sourceAnnotationValue.getDefaultValue().trim().isEmpty()) {
            return sourceAnnotationValue.getDefaultValue();
        }
        if (Objects.nonNull(targetAnnotationValue) && Objects.nonNull(targetAnnotationValue.getDefaultValue()) && !targetAnnotationValue.getDefaultValue().trim().isEmpty()) {
            return targetAnnotationValue.getDefaultValue();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private List<FieldAndMethod> getFieldList(Map<ElementKind, List<Element>> elementMap, TypeElement typeElement, MethodType needMethodType) {
        List<Element> filedElementList = elementMap.get(ElementKind.FIELD);
        List<Element> methodElementList = elementMap.get(ElementKind.METHOD);

        List<FieldAndMethod> fieldAndMethodList = new ArrayList<>();

        filedElementList.forEach(filedElement -> {
            List<? extends AnnotationMirror> mirrors = filedElement.getAnnotationMirrors();
            List<AnnotationMirror> mirrorList = new ArrayList<>();
            for (AnnotationMirror mirror : mirrors) {
                TypeElement annotationElement = (TypeElement) mirror.getAnnotationType().asElement();
                Name qualifiedName = annotationElement.getQualifiedName();
                if (RapidMappingField.class.getName().equals(qualifiedName.toString())) {
                    mirrorList.add(mirror);
                }
                if (RapidMappingFieldContainer.class.getName().equals(qualifiedName.toString())) {
                    for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> pEntry : mirror.getElementValues().entrySet()) {
                        if (!"value".contentEquals(pEntry.getKey().getSimpleName())) {
                            continue;
                        }
                        Object value = pEntry.getValue().getValue();
                        if (value instanceof List) {
                            mirrorList.addAll((List<AnnotationMirror>) value);
                        }
                    }
                }
            }

            FieldAndMethod fieldAndMethod = new FieldAndMethod();
            fieldAndMethod.setMethodType(needMethodType);
            fieldAndMethod.setFieldElement(filedElement);

            String methodName = getMethodName(methodElementList, filedElement, typeElement, needMethodType);
            if (Objects.isNull(methodName)) {
                return;
            }
            fieldAndMethod.setMethodName(methodName);

            Map<String, FieldAnnotationValue> annotationValueMap = mirrorList.stream().map(annotationMirror -> {
                        FieldAnnotationValue fieldAnnotationValue = new FieldAnnotationValue();
                        annotationMirror.getElementValues().entrySet().forEach(entry -> {
                            String name = entry.getKey().getSimpleName().toString();
                            Object value = entry.getValue().getValue();
                            switch (name) {
                                case "value":
                                    fieldAnnotationValue.setValue(String.valueOf(value));
                                    break;
                                case "targetClass":
                                    fieldAnnotationValue.setTargetClassName(String.valueOf(value));
                                    break;
                                case "ignore":
                                    fieldAnnotationValue.setIgnore((Boolean) value);
                                    break;
                                case "dateFormat":
                                    fieldAnnotationValue.setDateFormat(String.valueOf(value));
                                    break;
                                case "defaultValue":
                                    fieldAnnotationValue.setDefaultValue(String.valueOf(value));
                                    break;
                                case "required":
                                    fieldAnnotationValue.setRequired((Boolean) value);
                                    break;
                            }
                        });
                        String targetClassName = fieldAnnotationValue.getTargetClassName();
                        if (Objects.isNull(fieldAndMethod.getDefaultAnnotationValue()) &&
                                (Objects.isNull(targetClassName) || targetClassName.equals(Void.class.getName()))) {
                            fieldAndMethod.setDefaultAnnotationValue(fieldAnnotationValue);
                        }
                        return fieldAnnotationValue;
                    }).filter(fieldAnnotationValue ->
                            Objects.nonNull(fieldAnnotationValue.getTargetClassName()) &&
                                    !fieldAnnotationValue.getTargetClassName().equals(Void.class.getName()))
                    .collect(Collectors.toMap(FieldAnnotationValue::getTargetClassName, Function.identity(), (a, b) -> a));
            fieldAndMethod.setAnnotationValueMap(annotationValueMap);
            fieldAndMethodList.add(fieldAndMethod);
        });
        return fieldAndMethodList;
    }

    private String getMethodName(List<Element> methodElementList, Element filedElement, TypeElement typeElement, MethodType needMethodType) {
        String fieldName = filedElement.getSimpleName().toString();

        //由于Lombok可能未编译，如果存在Lombok对应的注解，这直接返回对应方法
        List<String> annotationMirrorList = typeElement.getAnnotationMirrors().stream().map(annotationMirror -> annotationMirror.toString()).collect(Collectors.toList());
        switch (needMethodType) {
            case READ:
                if (annotationMirrorList.contains("@lombok.Data") || annotationMirrorList.contains("@lombok.Getter")) {
                    return "boolean".equals(filedElement.asType().toString()) ? "is" + capitalizeFirstLetter(fieldName) : "get" + capitalizeFirstLetter(fieldName);
                }
            case WRITE:
                if (annotationMirrorList.contains("@lombok.Data") || annotationMirrorList.contains("@lombok.Setter")) {
                    return "set" + capitalizeFirstLetter(fieldName);
                }
        }

        for (Element methodElement : methodElementList) {
            String methodName = methodElement.getSimpleName().toString();
            switch (needMethodType) {
                case READ:
                    if ((methodName.equals("get" + capitalizeFirstLetter(fieldName))
                            || methodName.equals("is" + capitalizeFirstLetter(fieldName)))
                            && typeUtils.isSameType(((ExecutableElement) methodElement).getReturnType(), filedElement.asType())) {
                        return methodElement.getSimpleName().toString();
                    }
                    break;
                case WRITE:
                    if (methodName.equals("set" + capitalizeFirstLetter(fieldName)) && ((ExecutableElement) methodElement).getParameters().size() == 1) {
                        VariableElement variableElement = ((ExecutableElement) methodElement).getParameters().get(0);
                        if (typeUtils.isSameType(variableElement.asType(), filedElement.asType())) {
                            return methodElement.getSimpleName().toString();
                        }
                    }
                    break;
            }

        }
        return null;
    }

    public static String capitalizeFirstLetter(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    private void addElementList(TypeElement typeElement, List<Element> elementList) {
        if (Object.class.getSimpleName().contentEquals(typeElement.getSimpleName())) {
            return;
        }
        List<? extends Element> enclosedElements = typeElement.getEnclosedElements();
        elementList.addAll(enclosedElements);
        TypeElement superElement = (TypeElement) ((DeclaredType) typeElement.getSuperclass()).asElement();
        addElementList(superElement, elementList);
    }

    private boolean isSameOrSubtype(TypeMirror typeMirror, TypeMirror pTypeMirror) {
        if (typeUtils.isSameType(typeMirror, pTypeMirror)) {
            // 直接是 Enum 类型
            return true;
        } else {
            // 递归检查超类
            List<? extends TypeMirror> typeMirrors = typeUtils.directSupertypes(typeMirror);
            for (TypeMirror superTypeMirror : typeMirrors) {
                if (isSameOrSubtype(superTypeMirror, pTypeMirror)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isEnumOrSubtypeOfEnum(TypeMirror type, TypeElement enumElement) {
        if (type.getKind() == TypeKind.DECLARED) {
            DeclaredType declaredType = (DeclaredType) type;
            TypeElement typeElement = (TypeElement) declaredType.asElement();
            if (typeUtils.isSameType(typeElement.asType(), enumElement.asType())) {
                // 直接是 Enum 类型
                return true;
            } else {
                // 递归检查超类
                List<? extends TypeMirror> typeMirrors = typeUtils.directSupertypes(type);
                for (TypeMirror typeMirror : typeMirrors) {
                    if (isEnumOrSubtypeOfEnum(typeMirror, enumElement)) {
                        return true;
                    }
                    return false;
                }

            }
        }
        return false;
    }
}