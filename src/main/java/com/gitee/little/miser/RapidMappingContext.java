package com.gitee.little.miser;

import com.gitee.little.miser.converter.BaseConverter;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: wanxuefeng
 * @Description: 高性能映射上下文
 * @CreateTime: 2023-08-15  16:03
 */

public class RapidMappingContext {
    public static final Map<Class<?>, Map<Class<?>, BaseConverter>> CONVERTER_CACHE = new HashMap<>();

}
