package com.gitee.little.miser.converter;

public interface BaseConverter {
    void convert(Object sourceObj, Object targetObj, Boolean emptyCoverFlag);
}
