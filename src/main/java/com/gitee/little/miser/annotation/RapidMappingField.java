package com.gitee.little.miser.annotation;


import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Repeatable(RapidMappingFieldContainer.class)
public @interface RapidMappingField {

    //需转后属性名称
    String value() default "";

    //对应的类
    Class targetClass() default Void.class;

    //是否忽略
    boolean ignore() default false;

    //时间格式化
    String dateFormat() default "";

    //由于注解中无法使用复杂对象，默认值暂时只支持 String类型
    String defaultValue() default "";

    boolean required() default false;
}
