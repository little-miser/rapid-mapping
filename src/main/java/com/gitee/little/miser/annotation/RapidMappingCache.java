package com.gitee.little.miser.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RapidMappingCache {
    //被转换类class
    Class source();
    //目标类class
    Class target();
}
