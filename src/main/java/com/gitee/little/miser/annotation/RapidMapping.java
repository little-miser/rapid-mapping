package com.gitee.little.miser.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RapidMapping {

    //需要转换的class
    Class[] value() default {};

    Class[] source() default {};

    boolean required() default false;
}
