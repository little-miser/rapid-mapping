package com.gitee.little.miser.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface RapidMappingFieldContainer {
       RapidMappingField[] value();
}
