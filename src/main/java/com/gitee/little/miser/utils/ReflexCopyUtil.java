package com.gitee.little.miser.utils;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ReflexCopyUtil {

    protected static void copyProperties(Object target, Object source) {
        Class<?> targetClass = target.getClass();
        Class<?> sourceClass = source.getClass();
        try {
            //获取当前类中包含的所有属性
            Field[] fields = sourceClass.getDeclaredFields();
            for (Field field : fields) {
                //拼接获取setter/getter方法的名称
                String setMethodName = "set" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
                String getMethodName = "get" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
                //根据方法名称获取方法对象
                Method setMethod = targetClass.getMethod(setMethodName, field.getType());//setter方法
                Method getMethod = sourceClass.getMethod(getMethodName);//getter方法
                //执行源对象指定属性的getter方法，获取属性值
                Object returnVal = getMethod.invoke(source);
                //执行新对象的setter方法，将源对象的属性值设置给新对象
                setMethod.invoke(target, returnVal);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
