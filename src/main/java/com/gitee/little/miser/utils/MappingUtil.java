package com.gitee.little.miser.utils;

import com.gitee.little.miser.RapidMappingContext;
import com.gitee.little.miser.annotation.RapidMappingCache;
import com.gitee.little.miser.converter.BaseConverter;
import com.gitee.little.miser.exception.RapidMappingException;
import org.reflections.Reflections;

import java.util.*;

/**
 * @Author: wanxuefeng
 * @Description: 映射工具类
 * @CreateTime: 2023-08-15  10:58
 */
public class MappingUtil {

    private static boolean isInit = false;

    public static <T> T convert(Class<T> targetClass, Object sourceObj) {
        if (targetClass == null) {
            throw new RapidMappingException("创建转换对象类不能为空！");
        }
        if (sourceObj == null) {
            return null;
        }

        T targetObj = createTargetObj(targetClass);

        convert(targetObj, sourceObj, true);
        return targetObj;
    }

    public static <T> void convert(T targetObj, Object sourceObj) {
        convert(targetObj, sourceObj, true);
    }


    public static <T> List<T> convertList(List sourceObjList, Class<T> targetClass) {
        if (Objects.isNull(sourceObjList)||sourceObjList.isEmpty()){
            return new ArrayList<>();
        }
        List<T> targetList = new ArrayList<>(sourceObjList.size());

        sourceObjList.forEach(sourceObj -> {
            T target = convert(targetClass, sourceObj, true);
            targetList.add(target);
        });
        return targetList;
    }

    public static <T> T convert(Class<T> targetClass, Object sourceObj, boolean emptyCoverFlag) {
        if (targetClass == null) {
            throw new RapidMappingException("创建转换对象类不能为空！");
        }
        if (sourceObj == null) {
           return null;
        }

        T targetObj = createTargetObj(targetClass);
        convert(targetObj, sourceObj, true);
        return targetObj;
    }

    private static <T> T createTargetObj(Class<T> targetClass) {
        T targetObj = null;
        try {
            targetObj = targetClass.newInstance();
        } catch (Exception e) {
            throw new RapidMappingException("创建转换对象异常，异常对象为：" + targetClass.getName());
        }
        return targetObj;
    }

    public static <T> void convert(T targetObj, Object sourceObj, boolean emptyCoverFlag) {
        //判断是否初始化,没有初始化时，先进行初始化操作
        if (!isInit) {
            init();
        }

        //获取转换器
        Class<?> sourceClass = sourceObj.getClass();
        Class<?> targetClass = targetObj.getClass();
        Map<Class<?>, BaseConverter> converterMap = RapidMappingContext.CONVERTER_CACHE.get(sourceClass);
        BaseConverter converter = null;
        if (converterMap != null) {
            converter = converterMap.get(targetClass);
        }
        //当转换器不存在是通过反射进行copy
        if (converter != null) {
            converter.convert(sourceObj, targetObj, emptyCoverFlag);
            return;
        }
        ReflexCopyUtil.copyProperties(targetObj, sourceObj);
    }

    public static <T> T convertMoreToOne(Class<T> targetClass, Object... sourceObjs) {
        //判断是否初始化,没有初始化时，先进行初始化操作
        if (targetClass == null) {
            throw new RapidMappingException("创建转换对象类不能为空！");
        }
        if (sourceObjs == null || sourceObjs.length == 0) {
            return null;
        }

        T targetObj = createTargetObj(targetClass);

        convertMoreToOne(targetObj, sourceObjs);
        return targetObj;

    }

    public static <T> void convertMoreToOne(T targetObj, Object... sourceObjs) {
        //判断是否初始化,没有初始化时，先进行初始化操作
        if (Objects.isNull(targetObj)) {
            throw new RapidMappingException("创建转换对象类不能为空！");
        }
        if (sourceObjs == null || sourceObjs.length == 0) {
           return;
        }
        //判断是否初始化,没有初始化时，先进行初始化操作
        if (!isInit) {
            init();
        }

        //获取转换器
        for (Object sourceObj : sourceObjs) {
            Class<?> sourceClass = sourceObj.getClass();
            Class<?> targetClass = targetObj.getClass();
            Map<Class<?>, BaseConverter> converterMap = RapidMappingContext.CONVERTER_CACHE.get(sourceClass);
            BaseConverter converter = null;
            if (converterMap != null) {
                converter = converterMap.get(targetClass);
            }
            //当转换器不存在是通过反射进行copy
            if (converter != null) {
                converter.convert(sourceObj, targetObj, false);
                return;
            }
            ReflexCopyUtil.copyProperties(targetObj, sourceObj);
        }
    }


    public static synchronized void init() {
        if (isInit) {
            return;
        }
        isInit = true;
        //反射
        Reflections ref = new Reflections(BaseConverter.class.getPackage().getName());
        // 获取扫描到的标记注解的集合
        Set<Class<?>> set = ref.getTypesAnnotatedWith(RapidMappingCache.class);
        for (Class<?> clazz : set) {
            if (!BaseConverter.class.isAssignableFrom(clazz)) {
                continue;
            }
            RapidMappingCache annotation = clazz.getAnnotation(RapidMappingCache.class);
            if (Objects.nonNull(annotation)) {
                Class<?> source = annotation.source();
                Class<?> target = annotation.target();
                BaseConverter converter = null;
                try {
                    converter = (BaseConverter) clazz.newInstance();
                } catch (Exception e) {
                    throw new RapidMappingException("创建转换器异常", e);
                }
                Map<Class<?>, BaseConverter> converterMap = RapidMappingContext.CONVERTER_CACHE.get(source);
                if (Objects.isNull(converterMap)) {
                    converterMap = new HashMap<>();
                    RapidMappingContext.CONVERTER_CACHE.put(source, converterMap);
                }
                converterMap.put(target, converter);
            }
        }
    }
}
